package app

import (
	"context"
	"fmt"
	"time"

	"github.com/jpillora/backoff"
	"github.com/sirupsen/logrus"

	"github.com/patrickmn/go-cache"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sClient "k8s.io/client-go/kubernetes"
	typedv1 "k8s.io/client-go/kubernetes/typed/core/v1"

	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/config"
	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/logging"
)

var (
	PropagationPolicy = metav1.DeletePropagationForeground
)

type backoffCalculator interface {
	ForAttempt(attempt float64) time.Duration
}

type managerError struct {
	msg   string
	inner error
}

func newManagerError(metadata string, inner error) *managerError {
	return &managerError{metadata, inner}
}

func (m *managerError) Error() string {
	return fmt.Sprintf("manager encountered an error: %s, %s", m.inner.Error(), m.msg)
}

type PodsManager struct {
	backoff backoffCalculator

	logger logging.Logger
	cfg    config.Config

	podClient typedv1.PodInterface

	requestTimeout time.Duration

	// faultyPods is used to save the pods which cannot be deleted and the datetime they were found in the podList
	faultyPods *cache.Cache
}

type newPodsManagerOpts struct {
	logger     logging.Logger
	cfg        config.Config
	namespace  string
	kubeClient *k8sClient.Clientset
	backoff    *backoff.Backoff
}

func NewPodsManager(opts newPodsManagerOpts) (*PodsManager, error) {
	cacheCleanupInterval, err := opts.cfg.GetCacheCleanupInterval()
	if err != nil {
		return nil, err
	}

	cacheExpiration, err := opts.cfg.GetCacheExpiration()
	if err != nil {
		return nil, err
	}

	requestTimeout, err := opts.cfg.GetRequestTimeout()
	if err != nil {
		return nil, err
	}

	managerBackoff := opts.backoff
	if managerBackoff == nil {
		managerBackoff = &backoff.Backoff{Min: time.Second, Max: 30 * time.Second}
	}

	return &PodsManager{
		backoff:        managerBackoff,
		logger:         opts.logger.WithField("namespace", opts.namespace),
		cfg:            opts.cfg,
		podClient:      opts.kubeClient.CoreV1().Pods(opts.namespace),
		requestTimeout: requestTimeout,
		faultyPods:     cache.New(cacheExpiration, cacheCleanupInterval),
	}, nil
}

func (pm *PodsManager) Clean(ctx context.Context) error {
	duration, err := pm.cfg.GetInterval()
	if err != nil {
		return err
	}

	for {
		// During the pods deletion, when an error is returned (mainly due to failure to delete a pod)
		// the duration period before next retry is defined by the backoff duration.
		// When deletion happened as planned, the interval configured is used instead
		pm.logger.Debug("the deletion has started")
		err := pm.executeDeletion(ctx)
		if err != nil {
			pm.logger.Error(err)
		}
		pm.logger.Debug("the deletion is finished")

		select {
		case <-ctx.Done():
			err := ctx.Err()
			pm.logger.Error(err)
			return err
		case <-time.After(duration):
		}
	}
}

// retrieveAllPods get all the pods
// We do not set the limit in the metav1.ListOptions to avoid getting the same X first pods
// which might not be tagged for deletion until they eventually are deleted
func (pm *PodsManager) retrieveAllPods(ctx context.Context, continueToken string) (*corev1.PodList, error) {
	duration, err := pm.cfg.GetRequestTimeout()
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithTimeout(ctx, duration)
	defer cancel()

	pods, err := pm.podClient.List(
		ctx,
		metav1.ListOptions{
			TypeMeta: metav1.TypeMeta{
				Kind:       pods,
				APIVersion: podsAPIVersion,
			},
			Limit:    int64(pm.cfg.Kubernetes.RequestLimit),
			Continue: continueToken,
		},
	)
	if err != nil {
		return nil, newManagerError("retrieving pod list", err)
	}

	return pods, nil
}

// executeDeletion is used to delete the pods fitting the required condition
// Annotations are unfortunately not among the FieldsSelector which could have allowed a search of Pods using conditions
// Therefore the deletion conditions have to be implemented in the client
//
//nolint:gocognit
func (pm *PodsManager) executeDeletion(ctx context.Context) error {
	// At the beginning of each deletion run the beginning time is saved
	// This time is then used to identify the faultyPods which didn't appear in the ongoing deletion run
	executionStart := time.Now()
	defer pm.cleanupFaultyPods(executionStart)

	maxPodsToDelete := pm.cfg.Limit

	var amountOfDeletedPods int
	var continueToken string

	for {
		podList, err := pm.retrieveAllPods(ctx, continueToken)
		if err != nil {
			return err
		}

		continueToken = podList.GetContinue()

		for _, pod := range podList.Items {
			if amountOfDeletedPods >= pm.cfg.Limit {
				pm.logger.Info("limit of pods to delete reached")
				break
			}

			if pm.deletePod(ctx, pod) {
				amountOfDeletedPods++
			}
		}

		if amountOfDeletedPods >= maxPodsToDelete || continueToken == "" {
			return nil
		}
	}
}

func (pm *PodsManager) deletePod(ctx context.Context, pod corev1.Pod) bool {
	// The pod has already been marked for deletion: because of the Foreground Propagacy Policy
	if pod.DeletionTimestamp != nil {
		return false
	}

	// The pod failed to be deleted previously, so we skip it now
	if _, podIsFaulty := pm.faultyPods.Get(pod.GetName()); podIsFaulty {
		return false
	}

	annotation := pm.cfg.Kubernetes.Annotation
	if !pm.canDeletePod(pod, annotation) {
		return false
	}

	log := pm.logger.WithFields(logrus.Fields{
		"name":       pod.GetName(),
		"created_at": pod.CreationTimestamp,
		"annotation": annotation,
		"ttl":        pod.Annotations[annotation],
	})

	err := retry(ctx, pm.cfg.MaxErrAllowed, pm.backoff, log, func() error {
		c, cancel := context.WithTimeout(ctx, pm.requestTimeout)
		defer cancel()

		err := pm.podClient.Delete(c, pod.GetName(), metav1.DeleteOptions{PropagationPolicy: &PropagationPolicy})
		if err != nil {
			return newManagerError(fmt.Sprintf("error deleting pod %s", pod.GetName()), err)
		}

		return nil
	})
	if err != nil {
		pm.faultyPods.SetDefault(pod.GetName(), time.Now())
		return false
	}

	log.Info("pod successfully deleted")
	return true
}

// deletePods go through the list of pods retrieve from the cluster and try to delete them if possible
// When a pod is elligible to deletion, cfg.MaxErrAllowed attempts are made to delete the pod
// When the pod cannot be deleted, it is added in the faulty list of pods to be skipped during the next run.
// It returns the number of Pods actually deleted.
func (pm *PodsManager) canDeletePod(pod corev1.Pod, annotation string) bool {
	value, ok := pod.Annotations[annotation]
	if !ok {
		return false
	}

	l := pm.logger.WithFields(logrus.Fields{
		"name":       pod.GetName(),
		"created_at": pod.CreationTimestamp,
		"annotation": annotation,
		"ttl":        value,
	})

	ttl, err := time.ParseDuration(value)
	if err != nil {
		l.Error(fmt.Errorf("invalid TTL in pod annotations: %w", err))
		return false
	}

	if pod.CreationTimestamp.Add(ttl).UTC().Before(time.Now().UTC()) {
		return true
	}

	l.Debug("cannot delete pod as ttl not yet expired")

	return false
}

// cleanupFaultyPods remove from the faultyPods list those which are not anymore visible in the k8s cluster.
func (pm *PodsManager) cleanupFaultyPods(referenceTime time.Time) {
	for podName, item := range pm.faultyPods.Items() {
		if referenceTime.After(item.Object.(time.Time)) {
			pm.faultyPods.Delete(podName)
		}
	}
}

func retry(
	ctx context.Context,
	attempts int,
	backoff backoffCalculator,
	logger logging.Logger,
	f func() error,
) error {
	var (
		err             error
		backoffDuration time.Duration
		i               int = 0
	)

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-time.After(backoffDuration):
			err = f()
			if err == nil {
				return nil
			}

			logger.Error(err)
			i++
			if i > attempts-1 {
				return err
			}

			backoffDuration = backoff.ForAttempt(float64(i))
			logger.Debug(fmt.Sprintf(
				"Backing off pod deletion for %s because of the error: %s (attempt %d)",
				backoffDuration,
				err,
				i,
			))
		}
	}
}
