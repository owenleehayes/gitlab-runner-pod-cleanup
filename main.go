package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/app"
	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/config"
	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/logging"
)

var (
	NAME     = "Kubernetes Pod Garbage Collector"
	VERSION  = "dev"
	REVISION = "HEAD"
	BRANCH   = "HEAD"
	BUILT    = ""
)

type VersionInfo struct {
	Name         string
	Version      string
	Revision     string
	Branch       string
	GOVersion    string
	BuiltAt      string
	OS           string
	Architecture string
}

func (v *VersionInfo) String() string {
	version := fmt.Sprintln(v.Name)
	version += fmt.Sprintf("Version:      %s\n", v.Version)
	version += fmt.Sprintf("Git revision: %s\n", v.Revision)
	version += fmt.Sprintf("Git branch:   %s\n", v.Branch)
	version += fmt.Sprintf("GO version:   %s\n", v.GOVersion)
	version += fmt.Sprintf("Built:        %s\n", v.BuiltAt)
	version += fmt.Sprintf("OS/Arch:      %s/%s\n", v.OS, v.Architecture)

	return version
}

func (v *VersionInfo) UserAgent() string {
	return fmt.Sprintf("%s %s (%s)", v.Name, v.Version, v.Branch)
}

func newVersion() *VersionInfo {
	built := BUILT
	if built == "" {
		built = time.Now().UTC().Format(time.RFC3339)
	}

	version := &VersionInfo{
		Name:         NAME,
		Version:      VERSION,
		Revision:     REVISION,
		Branch:       BRANCH,
		GOVersion:    runtime.Version(),
		BuiltAt:      built,
		OS:           runtime.GOOS,
		Architecture: runtime.GOARCH,
	}

	return version
}

func main() {
	c := config.New()
	logger := logging.New()

	err := logger.SetFormat(c.LogFormat)
	if err != nil {
		logger.Fatal(err.Error())
	}
	err = logger.SetLevel(c.LogLevel)
	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Debug(c)

	ctx, cancel := context.WithCancel(context.Background())

	sigs := make(chan os.Signal, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	cleaner, err := app.NewCleaner(c, logger, newVersion().UserAgent())
	if err != nil {
		logger.Fatal(err.Error())
	}

	go func() {
		sig := <-sigs
		logger.Info("Signal received ", sig)
		cancel()
	}()

	errCh := make(chan error)
	go func() {
		err := cleaner.Clean(ctx)
		errCh <- err
	}()

	select {
	case <-ctx.Done():
		logger.Error(ctx.Err())
	case err := <-errCh:
		if err != nil {
			logger.Error(err)
		}
	}
}
